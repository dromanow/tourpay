# -*- coding: utf-8 -*-


from django.db import models


class Region(models.Model):
    name = models.CharField(verbose_name=u'Название', max_length=120)

    def __unicode__(self):
            return self.name

    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'


class City(models.Model):
    name = models.CharField(verbose_name=u'Название', max_length=120)
    region = models.ForeignKey(Region)

    def __unicode__(self):
            return self.name

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class Metro(models.Model):
    name = models.CharField(verbose_name=u'Название', max_length=120)
    city = models.ForeignKey(City)

    def __unicode__(self):
            return self.name

    class Meta:
        verbose_name = 'Станция метро'
        verbose_name_plural = 'Станции метро'
