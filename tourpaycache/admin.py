from django.contrib import admin
from tourpaycache.models import *

admin.site.register(Region)
admin.site.register(City)
admin.site.register(Metro)
