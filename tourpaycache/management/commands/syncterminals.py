__author__ = 'Denis Romanow'

import requests
import json
from django.core.management.base import BaseCommand
from tourpaycache.models import *


class Command(BaseCommand):
    help = 'Sync terminals database'

    def handle(self, *args, **options):
        r = requests.get('http://billing.tourpay.ru/terminals/FindRegions')
        regions = json.loads(r.text[1:-2])
        Region.objects.all().delete()
        City.objects.all().delete()
        Metro.objects.all().delete()
        for reg_data in regions['Regions']:
            reg = Region(name=reg_data['Name'])
            reg.save()
            for city_data in reg_data['Cities']:
                city = City(name=city_data['Name'], region=reg)
                city.save()
                for metro_data in city_data['Metros']:
                    metro = Metro(name=metro_data['Name'], city=city)
                    metro.save()
            self.stdout.write(u'Added region "%s"' % reg.name)

        for city in City.objects.all():
            r = requests.get('https://billing.tourpay.ru/terminals/FindTerminals?location_type=1&location_type=2&'
                             'location_type=3&status=True&status=False&status=&worked=False&worked=True&'
                             'worked=', params={'callback': 'c', 'city': city.name, })
            terminals = json.loads(r.text[2:-2])['Cities'][0]['Terminals']
            for terminal_data in terminals:
                pass
