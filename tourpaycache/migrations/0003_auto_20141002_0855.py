# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tourpaycache', '0002_auto_20141001_1055'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='city',
            options={'verbose_name': '\u0413\u043e\u0440\u043e\u0434', 'verbose_name_plural': '\u0413\u043e\u0440\u043e\u0434\u0430'},
        ),
        migrations.AlterModelOptions(
            name='metro',
            options={'verbose_name': '\u0421\u0442\u0430\u043d\u0446\u0438\u044f \u043c\u0435\u0442\u0440\u043e', 'verbose_name_plural': '\u0421\u0442\u0430\u043d\u0446\u0438\u0438 \u043c\u0435\u0442\u0440\u043e'},
        ),
        migrations.AlterModelOptions(
            name='region',
            options={'verbose_name': '\u0420\u0435\u0433\u0438\u043e\u043d', 'verbose_name_plural': '\u0420\u0435\u0433\u0438\u043e\u043d\u044b'},
        ),
    ]
