# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tourpaycache', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='metro',
            old_name='region',
            new_name='city',
        ),
    ]
