# -*- coding: utf-8 -*-


from django.db import models


class News(models.Model):
    title = models.CharField(max_length=140, verbose_name=u'Заголовок')
    preview = models.CharField(max_length=300, verbose_name=u'Анонс')
    text = models.TextField(verbose_name=u'Текст')
    date = models.DateField(verbose_name=u'Дата')
    public = models.BooleanField(verbose_name=u'Опубликовано', default=False)

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'

    def __unicode__(self):
        return self.title


class NewsImage(models.Model):
    img = models.ImageField(upload_to=u'images', verbose_name=u'Изображение')
    order = models.IntegerField(verbose_name=u'Порядок')
    news = models.ForeignKey(News)

    def __unicode__(self):
        return self.img.name

    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'
