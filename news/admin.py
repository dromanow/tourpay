from django.contrib import admin

from news.models import *


class NewsImageInLine(admin.TabularInline):
    model = NewsImage


class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'preview', 'public')
    list_editable = ('public', )
    inlines = [NewsImageInLine]

admin.site.register(News, NewsAdmin)
# admin.site.register(NewsImage)
